import { Component, OnInit } from '@angular/core';
import  *  as  reactassign  from  '../../reactassign.json';

@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.css']
})
export class SkillComponent implements OnInit {
  
  basics:any;
  constructor() { }

  ngOnInit() {
this.basics=reactassign.skills
  }
  clear(){
    window.localStorage.Clear();
  }
}
