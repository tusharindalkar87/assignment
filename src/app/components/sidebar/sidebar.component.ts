import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
public nameKey:any;
  constructor(
    public actRoute:ActivatedRoute,
    public router:Router
  ) { }

  ngOnInit() {
   this.nameKey= window.localStorage.getItem("key");
    console.log("nameKey ",this.nameKey);
    let ss=this.router.url;
    console.log("route ",ss);
    
  }

}
