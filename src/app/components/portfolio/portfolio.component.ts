import { Component, OnInit } from '@angular/core';
import  *  as  reactassign  from  '../../reactassign.json';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  clear(){
    window.localStorage.Clear();
  }
}
