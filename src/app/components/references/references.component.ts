import { Component, OnInit } from '@angular/core';
import  *  as  reactassign  from  '../../reactassign.json';

@Component({
  selector: 'app-references',
  templateUrl: './references.component.html',
  styleUrls: ['./references.component.css']
})
export class ReferencesComponent implements OnInit {

  basics:any;
  constructor() { }

  ngOnInit() {
this.basics=reactassign.references
  }

  clear(){
    window.localStorage.Clear();
  }
}
