import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    public router:Router,
    
  ) { }

  data=[
    {
      name:"HOME",
      id:"home"
    },
    {
      name:"ABOUT",
      id:"about"
    },
    {
      name:"WORK",
      id:"work"
    },
    {
      name:"EDUCATION",
      id:"education"
    },
    {
      name:"SKILLS",
      id:"skill"
    },
    {
      name:"PORTFOLIO",
      id:"portfolio"
    },
    {
      name:"REFERENCES",
      id:"references"
    }
  ]
  ngOnInit() {
   
  }
  getVal(data){ 
   
    window.localStorage.setItem("key",data.name);
    console.log("data ",data);
   
    this.router.navigate([data.id]);
    window.localStorage.clear
  }
  clear(){
    
  }
}
