import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {

  education= [
    {
      "institution": "The University of Pune",
      "area": "MSc. in Computer Science",
      "studyType": "Masters",
      "startDate": "2013-09-01",
      "endDate": "2014-12-31",
      "gpa": "Distinction",
      "summary": "Lorem Ipsum, Pune",
      "courses": [
        "Smart Phone Applications Development",
        "Financial Computing",
        "User Interface Design and Development",
        "Computation Intelligence and Machine Learning",
        "Web Technologies",
        "Data Mining",
        "Analysis and Design in UML",
        "Securities Transaction Banking",
        "Computational Finance",
        "Visualization and Visual Analytics"
      ]
    },
    {
      "institution": "Pune University of Science and Technology",
      "area": "BEng. in Computer Science",
      "studyType": "Bachelors",
      "startDate": "2007-09-01",
      "endDate": "2010-12-31",
      "gpa": "Honours",
      "summary": "Clear Water Bay, Pune",
      "courses": [
        "Computer Graphics",
        "Java Programming",
        "Database Management Systems",
        "Global Information Infrastructure and Policy",
        "Design and Analysis of Algorithms",
        "Operating Systems",
        "Probability and Random Processes in Engineering",
        "Discrete Mathematics",
        "Linear Algebra",
        "Internet Computing"
      ]
    },
    {
      "institution": "Pune University",
      "area": "BEng. in Computer Science",
      "studyType": "Bachelors",
      "startDate": "2009-09-01",
      "endDate": "2009-12-31",
      "gpa": "",
      "summary": "Vancouver, Pune",
      "courses": [
        "Software Engineering"
      ]
    }
  ]
  constructor() { }

  ngOnInit() {
  }

  clear(){
    window.localStorage.Clear();
  }
 
}
